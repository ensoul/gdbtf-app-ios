//
//  ApplicationHelper.swift
//  GDBTF
//
//   13/09/16.
//  
//

import UIKit
import Foundation

class ApplicationHelper{

    static func hexStringToUIColor (_ hex:String) -> UIColor {
        
        //var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewline)//.uppercased()
        
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespaces).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        if ((cString.characters.count) == 8) {
        
            return UIColor(
                
                red: CGFloat   ((rgbValue & 0x00FF0000) >> 16) / 255.0,
                green: CGFloat ((rgbValue & 0x0000FF00) >> 8)  / 255.0,
                blue: CGFloat  (rgbValue &  0x000000FF)        / 255.0,
                alpha: CGFloat ((rgbValue & 0xFF000000) >> 24) / 255.0
                
            )
        }
        
        if ((cString.characters.count) == 6) {
            
            return UIColor(
                
                red: CGFloat   ((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat ((rgbValue & 0x00FF00) >> 8)  / 255.0,
                blue: CGFloat  (rgbValue &  0x0000FF)        / 255.0,
                alpha: CGFloat(1.0)
            )
        }
        
        return UIColor.gray
    }
    
    static func getPreference(_ key:String) -> String {
    
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: key) == nil {
            //  Doesn't exist
        } else {
            return preferences.string(forKey: key)!
        }
        
        return "";
    }
    
    static func setPreference(_ key:String, _ value:String){
    
        let preferences = UserDefaults.standard
        
        preferences.set(value, forKey: key)
        
        preferences.synchronize()
    }
    
    static func removeSharedPreferences(_ key:String){
    
    
        let preferences = UserDefaults.standard
        
        preferences.removeObject(forKey: key)
        
        preferences.synchronize()

    }
    
    static func clearPreferences(){
    
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
    }
    
}
