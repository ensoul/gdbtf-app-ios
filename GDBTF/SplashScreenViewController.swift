//
//  SplashScreenViewController.swift
//  GDBTF
//
//   13/01/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import Kingfisher

class SplashScreenViewController: UIViewController{

    @IBOutlet weak var ivSplash: UIImageView!
    
    override func viewDidLoad() {
    
        let url = URL(string: ApplicationSetup.ADVIMG)
        ivSplash.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "splash"))
        //ivSplash.kf.setImage(with: url)
    }
    
    
}
