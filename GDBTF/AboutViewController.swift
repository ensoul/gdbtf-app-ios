//
//  AboutViewController.swift
//  GDBTF
//
//   05/03/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class AboutViewController: UIViewController{
    
    @IBOutlet weak var ivArlef: UIImageView!
    @IBOutlet weak var tvClaap: UITextView!
    @IBOutlet weak var tvMailClaap: UITextView!
    
    override func viewDidLoad() {
     
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background_info.png")!.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch))
        
        let arlefGesture = UITapGestureRecognizer(target: self, action: #selector(AboutViewController.arlefTapped(gesture:)))
        let claapGesture = UITapGestureRecognizer(target: self, action: #selector(AboutViewController.claapTapped(gesture:)))
        let claapMailGesture = UITapGestureRecognizer(target: self, action: #selector(AboutViewController.claapMailTapped(gesture:)))

        ivArlef.addGestureRecognizer(arlefGesture)
        ivArlef.isUserInteractionEnabled = true
        
        tvClaap.addGestureRecognizer(claapGesture)
        tvClaap.isUserInteractionEnabled = true
        
        tvMailClaap.addGestureRecognizer(claapMailGesture)
        tvMailClaap.isUserInteractionEnabled = true
    }
    
    @objc func arlefTapped(gesture: UIGestureRecognizer) {
        
        let url = URL(string: "http://www.arlef.it")!
        openURL(url)
    }
    
    @objc func claapTapped(gesture: UIGestureRecognizer) {
        
        let url = URL(string: "http://www.claap.org")!
        openURL(url)
    }
    
    @objc func claapMailTapped(gesture: UIGestureRecognizer) {
        
        let url = URL(string: "arlef@regione.fvg.it")!
        openURL(url)
    }
    
    func openURL(_ url: URL){
    
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }

    }

}
