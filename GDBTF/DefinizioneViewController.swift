//
//  RicercaViewViewController.swift
//  GDBTF
//
//   03/08/16.
//  
//

import UIKit

class DefinizioneViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MyCustomCellDelegator {

    
    // String query = ApplicationSetup.ENGINEURL+"lemma?uid="+ricerca.getUid();
    
    var _definizioni:[Definizione] = [Definizione]()
    
    @IBOutlet weak var tvDefinizione: UITableView!
    @IBOutlet weak var lbDefinizioneRicerca: UILabel!
    
//    @IBOutlet weak var tvDefinizione: UITableView!
//    @IBOutlet weak var lbDefinizioneRicerca: UILabel!
    
    var lemma : Lemma?
    var _title = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvDefinizione.delegate = self
        tvDefinizione.dataSource = self
        
        tvDefinizione.rowHeight = UITableViewAutomaticDimension
        tvDefinizione.estimatedRowHeight = 100

        lbDefinizioneRicerca.text = lemma!.parola
  
        searchDefinizione()
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchDefinizione() {
        
        let query : String = ApplicationSetup.ENGINEURL+ApplicationSetup.LEMMA+lemma!.id!;
        
        
        if(!ConnectivityHelper.isInternetAvailable()){
            
            //ToastHelper.showAlert(backgroundColor: UIColor.lightGray, textColor: UIColor.black, message: "Offline")
            
            var result = ApplicationHelper.getPreference(query)
            
            var data = result.data(using: .utf8)!
            
            self.parseLEMMA(data)
            
            DispatchQueue.main.async {
                self.tvDefinizione.reloadData()
                self.messageFrame.removeFromSuperview()
                self.lbDefinizioneRicerca.text = self._title
            }
            
        }
        else{
            //let request = URLRequest(url: URL(string: query)!)
            
            self.progressBarDisplayer("Attendere", true)
            
            let url = URL(string: query)
            
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                
                guard error == nil else {
                    print(error)
                    return
                }
                guard let data = data else {
                    print("Data is empty")
                    return
                }
            
                let strData = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String
                //ApplicationHelper.setPreference(query, strData!)
                CacheFifo.putCache(query, strData!)
                
                self.parseLEMMA(data)
                
                DispatchQueue.main.async {
                    self.tvDefinizione.reloadData()
                    self.messageFrame.removeFromSuperview()
                    self.lbDefinizioneRicerca.text = self._title
                }
               
            }
            
            task.resume()
            
        }

    }
    
    func getStringFromArray(_ jObject:[String:AnyObject], _ name:String!) -> String?{
    
        var array = jObject[name] as! NSArray
        var result = ""
        
        if(array.count == 0){
            return ""
        }
        
        for i in 0...array.count-1 {
            
            if(i==array.count-1){
                result = result + (array[i] as! String)
            }
            else{
                result = result + (array[i] as! String) + ", "
            }
        }
        
        return result
    }
    
    func parseLEMMA(_ data : Data?){
        
        let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        if(ApplicationSetup.DEBUG){
            print(str)
        }
        
        do{
        
            let jObject = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject]
            
            var definizioni = [Definizione]()
            
            var lemma = jObject["lemma"] as? String
            var apex = jObject["apex"] as? String
            var cgs = TextUtils.removeQuotations(getStringFromArray(jObject, "cgs")!)
            var type = jObject["type"] as! Int
            
            if(apex != nil){
                apex = TextUtils.getApex(number: apex!)
            }
            else{
                apex = ""
            }
            
            _title = String(format: "(%@) %@", lemma!+apex!, cgs)
            
            //_title += lemma! + apex! + cgs
            
            var type_props = jObject["type_props"] as! [String: AnyObject]
            
            if(type == Ricerca.CONFISSO && type_props["etimology"] != nil){
                
                var etimology = type_props["etimology"] as! String
                _title += String(format: " [%@]", etimology)
            }
            
            if(type ==  Ricerca.LEMMA_MONOREMATICO){
            
                if(type_props["uas"] != nil){
                
                    var uas = type_props["uas"] as! NSArray
                    var has_roman = uas.count >  1
                    
                    var i = 1
                    for ua in uas as! [[String:AnyObject]]{
                    
                        if(has_roman){
                        
                            var defRoman = Definizione()
                            defRoman.romanIndex = i
                            defRoman.roman = true
                            definizioni.append(defRoman)
                            
                            i=i+1
                        }
                        var bas = ua["bas"] as! NSArray
                        parseBAS(bas, &definizioni, type);
                    }
                }
            }
            else
            {
                var bas =  type_props["bas"] as! NSArray
                parseBAS(bas, &definizioni, type);

            }
            
            _definizioni = definizioni
                
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    func parseBAS(_ bas:NSArray, _ definizioni:inout[Definizione], _ lemma_type:Int){
        
        for ba in bas as! [[String:AnyObject]]
        {
            var ds = [String:AnyObject]()
            
            if(lemma_type != Ricerca.CONFISSO){
                
                ds = ba["main_ds"] as! [String:AnyObject]
            }
            else{
                ds = ba["ds"] as! [String:AnyObject]
            }
            
            var definizione_result = ""
            var degree = ba["degree"] as! String
            var subdegree = ba["subdegree"] as! String
            
            definizione_result += String(degree) + subdegree + " "
            
            if(lemma_type != Ricerca.CONFISSO){
                
                var cgs = getStringFromArray(ba, "cgs")!
                definizione_result += TextUtils.removeBrackets(cgs)
            }
            else{
                
                var css = getStringFromArray(ba, "css")!
                css = TextUtils.removeBrackets(css)
                css = TextUtils.joinString(css)

                if(!TextUtils.isEmpty(css)){
                    definizione_result += "[TS] " + css
                }
            }
            
            var definizione = parseDS(ds, lemma_type)
            definizione.text1 = definizione_result + " " + definizione.text1
            definizione.degree =  String(degree)
            definizione.subdegree = subdegree
            
            if(lemma_type != Ricerca.CONFISSO){
                
//                if((ds["weak_forms"] as? [NSArray]) != nil)
//                {
                    var weak_forms = ds["weak_forms"] as? NSArray
                    //ds["weak_forms"] as? [String:AnyObject]
                
                if(weak_forms != nil){
                    
                    for weak_form in weak_forms as! [[String:AnyObject]]{
                    
                        var def_weak = parseDS(weak_form, lemma_type)
                        definizione.sottoDefinizioneList.append(def_weak)
                        
                        var weak_forms2 = weak_form["weak_form"] as? NSArray
                        
                        if(weak_forms2 != nil){
                        
                        for weak_form2 in weak_forms2 as! [[String:AnyObject]]{
                        
                            var def_weak2 = parseDS(weak_form2, lemma_type)
                            definizione.sottoDefinizioneList.append(def_weak2)
                        }
                        }
                    }
                }
//                }
            }
            
            definizioni.append(definizione)
        }
    }
    
    func parseDS(_ ds:[String:AnyObject], _ lemma_type:Int) -> Definizione{
    
        var definizione = Definizione()
    
        if(lemma_type != Ricerca.CONFISSO){
        
            var mus = getStringFromArray(ds, "mus")!
            mus = TextUtils.removeQuotations(mus)
            if(!mus.isEmpty){
                definizione.appendText1("[" + mus + "] ")
            }
            
            var css = getStringFromArray(ds, "css")
            var ccs =  getStringFromArray(ds, "ccs")
            
            css = css == nil ? "" : css
            ccs = ccs == nil ? "" : ccs
            
            css = TextUtils.removeBrackets(css!)
            css = TextUtils.joinString(css!)
            
            ccs = TextUtils.removeBrackets(ccs!)
            ccs = TextUtils.joinString(ccs!)
            
            definizione.appendText1(css! + ccs!)
        }
        
        var definition = ds["definition"] as? String
        
        //var translations = ds["translations"] as? String
        
        var translations = getStringFromArray(ds, "translations")
        
        definition = TextUtils.isEmpty(definition) ? "" : TextUtils.removeBrackets(definition!)
        translations = TextUtils.isEmpty(translations) ? "" : TextUtils.removeBrackets(translations!)
  
        definizione.definizione = translations
        definizione.appendText2("("+definition!+")" + " ")
        
        var sentences = ds["sentences"] as! NSArray
        
        for sentence in sentences as! [NSArray]{
        
            var frase_ita = sentence[0] as! String
            var frase_fur = sentence[1] as! String
            
            var frase =  Frase()
            frase.frase_ita = frase_ita
            frase.frase_fur = "\n" + frase_fur
            
            definizione.frasi.append(frase)
        }
        
        return definizione
    }
    
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    
    
    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _definizioni.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "DefinizioneTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DefinizioneTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let definizione = _definizioni[(indexPath as NSIndexPath).row]
        
        cell.renderDefinizione(definizione)
        cell.delegate = self
        cell.contentView.setNeedsLayout()
        cell.contentView.layoutIfNeeded()
        
        if ((indexPath as NSIndexPath).row % 2 == 0){
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#e0dad1")
        } else {
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#ede9e4")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow!
        
        let currentCell = tableView.cellForRow(at: indexPath) as! DefinizioneTableViewCell
        
    }
    
    func callSegueFromCell(myData dataobject: AnyObject) {

        var lemma = dataobject as! Lemma

        var storyBoard : UIStoryboard! = self.storyboard as UIStoryboard!
        let next = storyBoard.instantiateViewController(withIdentifier: "DefinizioneViewController") as? DefinizioneViewController
        next!.lemma = lemma
        self.navigationController?.pushViewController(next!, animated: true)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.destination is DefinizioneViewController){
            
            let destinationVC = segue.destination as? DefinizioneViewController
            
            var ricerca = sender as? Ricerca
            
            var lemma =  Lemma()
            lemma.id = ricerca?.uid
            lemma.parola = ricerca?.text
            
            destinationVC?.lemma = lemma
        }
        else if(segue.destination is LemmiViewController){
            
            let destinationVC = segue.destination as? LemmiViewController
            destinationVC?.ricerca = sender as? Ricerca
        }
    }
    



}
