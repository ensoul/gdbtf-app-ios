//
//  LemmiViewController.swift
//  GDBTF
//
//   26/09/16.
//  
//

import UIKit

class LemmiViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tvLemmi: UITableView!
    
    var ricerca : Ricerca?
    var _lemmi:[Lemma] = [Lemma]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvLemmi.delegate = self
        tvLemmi.dataSource = self
        
        //tvLemmi.rowHeight = UITableViewAutomaticDimension
        //tvLemmi.estimatedRowHeight = 180
        
        _lemmi = ricerca!.lemmi
        
        tvLemmi.reloadData()
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _lemmi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "LemmaTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LemmaTableViewCell
        
        if ((indexPath as NSIndexPath).row % 2 == 0){
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#e0dad1")
        } else {
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#ede9e4")
        }

        // Fetches the appropriate meal for the data source layout.
        let lemma = _lemmi[(indexPath as NSIndexPath).row]
        
        cell.setup(lemma)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow!
        
        let currentCell = tableView.cellForRow(at: indexPath) as! LemmaTableViewCell
        
        let lemmaSelected = _lemmi[(indexPath as NSIndexPath).row]
        
        var storyBoard : UIStoryboard! = self.storyboard as UIStoryboard!
        self.performSegue(withIdentifier: "DefinizioneViewSegue", sender: lemmaSelected)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as? DefinizioneViewController
        destinationVC?.lemma = sender as? Lemma
    }

}
