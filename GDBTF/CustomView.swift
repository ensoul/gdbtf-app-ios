//
//  CustomView.swift
//  GDBTF
//
//   08/12/2019.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import UIKit

class CustomView: UIView {
    var height = 1.0

    override open var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: 1.0, height: height)
        }
    }
}
