//
//  LemmaTableViewCell.swift
//  GDBTF
//
//   26/09/16.
//  
//

import Foundation
import UIKit

class LemmaTableViewCell : UITableViewCell {

    var _lemma: Lemma?
    
    @IBOutlet weak var lbLemma: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup(_ lemma: Lemma)
    {
        lbLemma.text = TextUtils.setupApex(lemma.parola!)
    }
}
