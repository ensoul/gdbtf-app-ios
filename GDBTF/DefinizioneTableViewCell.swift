//
//  DefinizioneTableViewCell.swift
//  GDBTF
//
//   09/08/16.
//  
//

import UIKit
import Foundation

class DefinizioneTableViewCell: UITableViewCell {
    
    var _labelList = [UILabel]()
    var _definizione: Definizione?
    var _button: UIButton!
    var _lemmiTags = Dictionary<Int, Lemma>()
    var _lemmiIndex = 0

    @IBOutlet weak var definizione_item_text_1: UILabel!
    @IBOutlet weak var definizione_item_text_2: UILabel!
    //@IBOutlet weak var definizione_item_text_3: UILabel!
    @IBOutlet weak var svMain: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        for i in 0..._labelList.count {
            
            if(i < _labelList.count){
                
                self.svMain.removeArrangedSubview(_labelList[i])
                _labelList[i].removeFromSuperview()
            }
            
        }
        
        if(_button != nil){
            self.svMain.removeArrangedSubview(_button)
            _button.removeFromSuperview()
        }
        _labelList = [UILabel]()
    }
    
    func renderDefinizione(_ definizione: Definizione)
    {
        renderDefinizione(definizione, nil, nil)
    }
    
    func renderDefinizione(_ definizione: Definizione, _ lbl1: UILabel?, _ lbl2: UILabel?)
    {
        var sottoDefinizione = false
        var label1 = definizione_item_text_1!
        var label2 = definizione_item_text_2!
     
        if(lbl1 != nil){
            label1 = lbl1!
            self.svMain.addArrangedSubview(label1)
            sottoDefinizione = true
        }
        
        if(lbl2 != nil){
            label2 = lbl2!
            self.svMain.addArrangedSubview(label2)
        }
     
        if(definizione.roman){
            
            label1.text = TextUtils.toRoman(number: definizione.romanIndex!)
            label1.textAlignment = NSTextAlignment.center
            label2.isHidden = true
            
            return
        }
        
        label1.textAlignment = NSTextAlignment.left
        label2.isHidden = false
        
        var fontNormal = UIFont.systemFont(ofSize: 17.0)
        var fontItalic = UIFont.italicSystemFont(ofSize: 17.0)
        var fontBold = UIFont.boldSystemFont(ofSize: 17.0)

        var mutableString = NSMutableAttributedString()
        
        if(!sottoDefinizione){
            if(definizione.text1.contains("[")){
                
                //1 s.m. [FO]
                
                let idxNormal = TextUtils.indexLast(where:definizione.text1, of:definizione.degree+definizione.subdegree)
                let idxItalic = TextUtils.indexFirst(where:definizione.text1, of:"[")
          
                mutableString = NSMutableAttributedString(
                    string: definizione.text1,
                    attributes: [NSAttributedStringKey.font: fontNormal] )
                
                mutableString.addAttribute(NSAttributedStringKey.font,
                                           value: fontBold,
                                           range: NSRange( location:0, length: idxItalic))
                
                mutableString.addAttribute(NSAttributedStringKey.font,
                                           value: fontItalic,
                                           range:  NSRange( location: idxNormal, length: idxItalic - idxNormal))
            }
            else
            {
                //1 s.m.
                
                let idxNormal = TextUtils.indexLast(where:definizione.text1, of:definizione.degree+definizione.subdegree)
                let idxItalic = definizione.text1.characters.count
          
                mutableString = NSMutableAttributedString(
                    string: definizione.text1,
                    attributes: [NSAttributedStringKey.font: fontNormal] )
                
                mutableString.addAttribute(NSAttributedStringKey.font,
                                           value: fontBold,
                                           range: NSRange( location:0, length: idxItalic))
                
                mutableString.addAttribute(NSAttributedStringKey.font,
                                           value: fontItalic,
                                           range:  NSRange( location: idxNormal, length: idxItalic - idxNormal))
                
            }
            
            label1.attributedText =  mutableString
        }
        else
        {
            label1.text =  definizione.text1
            
            //label1.backgroundColor = UIColor.lightGray
            //label2.backgroundColor = UIColor.lightGray
            //label3.backgroundColor = UIColor.lightGray
        }
        
        
        if(definizione.text2.contains("a href='#'"))
        {
            label2.text = ""

            _button = createButton(html:definizione.text2)
            self.svMain.addArrangedSubview(_button)
            
        }
        else{

            var ms2 = NSMutableAttributedString()
            var txt2a = definizione.text2
            var txt2b = " "+definizione.definizione!
            var txt2 = txt2a + txt2b
            
            var idx2a = TextUtils.indexLast(where: txt2, of: txt2a)
            
            
            ms2 = NSMutableAttributedString(
                string: txt2,
                attributes: [NSAttributedStringKey.font: fontItalic] )
            
            ms2.addAttribute(NSAttributedStringKey.foregroundColor,
                               value: ApplicationHelper.hexStringToUIColor("#003296"),
                               range: NSRange( location: idx2a, length: txt2.characters.count - idx2a))
            
            ms2.addAttribute(NSAttributedStringKey.font,
                             value: fontBold,
                             range: NSRange( location: idx2a, length: txt2.characters.count - idx2a))

            label2.attributedText = ms2
            
            for frase in definizione.frasi{
                renderFrase(frase)
            }
            
            for sottoDef in definizione.sottoDefinizioneList{

                var lbl1 = createLabel()
                var lbl2 = createLabel()

                renderDefinizione(sottoDef, lbl1,lbl2)
            }
        }

    }
    
    
    func renderFrase(_ frase:Frase){
    
        
        //"\nil cjan al vuiche <span class=\"o_ancje\">o ancje</span> il cjan al caìne"
        
        // ------------------1-----2--------------end
        //"il cjan al vuiche o ancje il cjan al caìne"

        var mutableString = NSMutableAttributedString()
        var fontNormal = UIFont.systemFont(ofSize: 17.0)
        var fontItalic = UIFont.italicSystemFont(ofSize: 17.0)
        var fontBold = UIFont.boldSystemFont(ofSize: 17.0)
        
        if(frase.frase_fur!.contains("<span")){
        
            var span = frase.frase_fur!.substring(from: TextUtils.indexLast(where: frase.frase_fur!, of: "\">"), to: TextUtils.indexFirst(where: frase.frase_fur!, of: "</span>"))
            
            var attrIta = try! NSAttributedString(
                data : frase.frase_ita!.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ .documentType: NSAttributedString.DocumentType.html ],
                documentAttributes: nil)
            
            var attrFur = try! NSAttributedString(
                data : frase.frase_fur!.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ .documentType: NSAttributedString.DocumentType.html ],
                documentAttributes: nil)
            
            let frase_ita_fur = "> " + attrIta.string + "\n" + attrFur.string
            
            var start = attrIta.string.characters.count + 2
            var end = frase_ita_fur.characters.count
            
            var idx1 = start + TextUtils.indexFirst(where: attrFur.string, of: span)+2

            mutableString = NSMutableAttributedString(
                string: frase_ita_fur,
                attributes: [NSAttributedStringKey.font: fontNormal] )
            
            mutableString.addAttribute(NSAttributedStringKey.font,
                                       value: fontBold,
                                       range: NSRange( location:0, length: "> ".characters.count))
            
            mutableString.addAttribute(NSAttributedStringKey.foregroundColor,
                                       value: ApplicationHelper.hexStringToUIColor("#003296"),
                                       range:  NSRange( location:0, length: "> ".characters.count))
            
            mutableString.addAttribute(NSAttributedStringKey.font,
                                       value: fontItalic,
                                       range: NSRange( location:start, length: end-start))
            
            mutableString.addAttribute(NSAttributedStringKey.foregroundColor,
                                       value: ApplicationHelper.hexStringToUIColor("#003296"),
                                       range:  NSRange( location: start, length: end-start))
            
            mutableString.addAttribute(NSAttributedStringKey.font,
                                       value: fontBold,
                                       range: NSRange( location:idx1, length: span.characters.count))
        }
        else{
            
            var attrIta = try! NSAttributedString(
                data : frase.frase_ita!.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ .documentType: NSAttributedString.DocumentType.html ],
                documentAttributes: nil)
            
            var attrFur = try! NSAttributedString(
                data : frase.frase_fur!.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                options: [ .documentType: NSAttributedString.DocumentType.html ],
                documentAttributes: nil)
            
            let frase_ita_fur = "> " + attrIta.string + "\n" + attrFur.string
            
            var start = attrIta.string.characters.count + 2
            var end = frase_ita_fur.characters.count
            
            mutableString = NSMutableAttributedString(
                string: frase_ita_fur,
                attributes: [NSAttributedStringKey.font: fontNormal] )
            
            mutableString.addAttribute(NSAttributedStringKey.font,
                                       value: fontBold,
                                       range: NSRange( location:0, length: "> ".characters.count))
            
            mutableString.addAttribute(NSAttributedStringKey.foregroundColor,
                                       value: ApplicationHelper.hexStringToUIColor("#003296"),
                                       range:  NSRange( location:0, length: "> ".characters.count))
            
            mutableString.addAttribute(NSAttributedStringKey.font,
                                       value: fontItalic,
                                       range: NSRange( location:start, length: end-start))
            
            mutableString.addAttribute(NSAttributedStringKey.foregroundColor,
                                       value: ApplicationHelper.hexStringToUIColor("#003296"),
                                       range:  NSRange( location: start, length: end-start))

        }
        
        var label = createLabel()
        label.attributedText = mutableString
        
        self.svMain.addArrangedSubview(label)
        
    }
    
    func createLabel() -> UILabel{
    
        let label = CustomLabel(frame: CGRect(x: 0, y: 10, width: 200, height: 21))
        label.textAlignment = NSTextAlignment.left
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont.italicSystemFont(ofSize: 17.0)
        label.textInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        _labelList.append(label)
        
        return label
    }
    
    func createButton(html: String) -> UIButton{
    
        //<a href='#' class='riflemma' id='2-1_26123'><span class='black'>nottolino</span></a> 
        
        var title = html.substring(from: TextUtils.indexLast(where: html, of: "'black'>"), to: TextUtils.indexFirst(where: html, of: "</span>"))
        var id = html.substring(from: TextUtils.indexLast(where: html, of: "-"), to: TextUtils.indexFirst(where: html, of: "'><span"))
        
        title = title.replacingOccurrences(of: "<sup>", with: "(")
        title = title.replacingOccurrences(of: "</sup>", with: ")")
        if(title.contains("(")){
            title = title.replacingOccurrences(of: "(1)", with: "") + TextUtils.setupApex(title)
        }
        
        var button:UIButton = UIButton(frame: CGRect(x: 250, y: 10, width: 400, height: 50))
        button.backgroundColor = ApplicationHelper.hexStringToUIColor("#A1211C")
        button.setTitle(title, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 1, left: 5, bottom: 1, right: 5)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

        var lemma =  Lemma()
        lemma.id = id
        lemma.parola = title
        
        button.tag = _lemmiIndex
        _lemmiTags[_lemmiIndex] = lemma
        
        _lemmiIndex = _lemmiIndex+1
        
        return button;
    }
    
    var delegate:MyCustomCellDelegator!
    
    @objc func buttonAction(sender: UIButton!) {
        
        var btnsendtag: UIButton = sender
        
        var lemma =  _lemmiTags[btnsendtag.tag]
        //lemma.id = "1_26147"
        //lemma.parola = "nottolino"

        if(self.delegate != nil){
            self.delegate.callSegueFromCell(myData: lemma!)
        }
    }
    

}
