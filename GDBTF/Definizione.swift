//
//  Definizione.swift
//  GDBTF
//
//   09/08/16.
//  
//

import Foundation

class Definizione{

    var roman = false
    var romanIndex : Int?
    var title : String?
    var cg : String?
    var text1  = ""
    var definizione : String?
    var text2 = ""
    //var text3 : String?
    var uid : String?
    var frasi:[Frase] = [Frase]()
    var sottoDefinizioneList:[Definizione] = [Definizione]()
    
    var degree = ""
    var subdegree = ""
    
    func appendText1(_ value:String){
        text1 = text1 + value
    }
    
    func appendText2(_ value:String){
        text2 = text2 + value
    }
    
}
