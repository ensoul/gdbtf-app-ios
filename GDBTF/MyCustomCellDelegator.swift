//
//  MyCustomCellDelegator.swift
//  GDBTF
//
//   16/12/16.
//  _
//

import Foundation


protocol MyCustomCellDelegator {
    func callSegueFromCell(myData dataobject: AnyObject)
}
