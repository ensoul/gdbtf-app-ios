//
//  NavigationBar.swift
//  GDBTF
//
//   06/03/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import UIKit

class NavigationBar : UINavigationBar {
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        
        let newSize :CGSize = CGSize(width: self.frame.size.width, height: 60)
        return newSize
    }
    
}
