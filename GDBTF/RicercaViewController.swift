//
//  ViewController.swift
//  GDBTF
//
//   19/07/16.
//  
//

import UIKit

class RicercaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    enum TipoRicerca{
    
        case ita,fur, undefined
    }
    
    var _tipoRicerca: TipoRicerca?
    var _task: URLSessionTask?;
    
    @IBOutlet weak var etRicerca: UITextField!
    @IBOutlet weak var tvRicerca: UITableView!
  
    @IBOutlet var tvRicercaConstraint: NSLayoutConstraint!
    @IBOutlet var keyboardStackView: UIStackView!
    
    var _pagineTotali = -1;
    var _paginaCorrente = -1;
    
    var _list:[Ricerca] = [Ricerca]()

    var isKeyboardVisible = false;
    
    @IBAction func virtualKeyboardToggle(_ sender: Any) {
    
         isKeyboardVisible = !isKeyboardVisible;
        
        if(!isKeyboardVisible)
        {
            keyboardStackView.isHidden = true;
            tvRicercaConstraint.constant = -30;
            
        }
        else{
            keyboardStackView.isHidden = false;
            tvRicercaConstraint.constant = 0;
            
        }
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvRicerca.delegate = self
        tvRicerca.dataSource = self
        
        etRicerca.clearButtonMode = .whileEditing;
        
        if(ApplicationSetup.DEBUG){
            printFonts()
            etRicerca.text = "alto"
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "indaûr", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        //navigationController?.navigationBar.barTintColor = ApplicationHelper.hexStringToUIColor("#c2b5a3")
        //tabBarController?.tabBar.barTintColor = ApplicationHelper.hexStringToUIColor("#c2b5a3")
        UINavigationBar.appearance().contentMode = UIViewContentMode.center
        //UINavigationBar.appearance().tintColor = ApplicationHelper.hexStringToUIColor("#c2b5a3")
        
        //UINavigationBar.appearance().backgroundColor = ApplicationHelper.hexStringToUIColor("#c2b5a3")
        let bounds = self.navigationController!.navigationBar.bounds
        var headerview = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
        let imgview = UIImageView(frame: CGRect(x:0, y: 0, width: 300, height: headerview.bounds.height))
        //imgview.center = headerview.center;
        
        imgview.image = UIImage(named: "action_bar_image")
        imgview.contentMode = UIViewContentMode.scaleAspectFit
        //imgview.contentMode = .center
        
        

        headerview.addSubview(imgview)
        
        //imgview.addConstraint(NSLayoutConstraint(item: headerview, attribute: .leading, relatedBy: .equal, toItem: imgview, attribute: .leading, multiplier: 1, constant: 0))
        
        //headerview.addConstraint(NSLayoutConstraint(item: imgview, attribute: .centerY, relatedBy: .equal, toItem: headerview, attribute: .centerY, multiplier: 1, constant: 0))
        //headerview.
        self.navigationController?.navigationBar.topItem?.titleView = headerview
        
        etRicerca.backgroundColor = ApplicationHelper.hexStringToUIColor("#ddd7ce")
        etRicerca.tintColor = UIColor.black
        
        //keyboardView.height = 1.0
        //ricercaView.height = 3.0
    }
    
    @IBAction func key1(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "à";
    }
    
    @IBAction func key2(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "â";
    }
    
    @IBAction func key3(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "è";
    }
    
    @IBAction func key4(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "ê";
    }
    
    @IBAction func key5(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "ì";
    }
    
    @IBAction func key6(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "î";
    }
    
    @IBAction func key7(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "ò";
    }
    
    @IBAction func key8(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "ô";
    }
    
    @IBAction func key9(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "ù";
    }
    
    @IBAction func key10(_ sender: Any) {
        etRicerca.text = etRicerca.text! + "û";
    }
    
    @IBAction func etRicercaChanged(_ sender: Any) {
        
        /*let searchWord = etRicerca.text!
        
        if(searchWord.characters.count == 0){
            _list.removeAll()
            tvRicerca.reloadData()
        }*/

        
    }
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func bnItaliano(_ sender: AnyObject) {
        
        _tipoRicerca = TipoRicerca.ita
        _paginaCorrente = 1;
        _list.removeAll()
        InviaRichiestaRicerca(String(_paginaCorrente))
    }
    
    /*@IBAction func contextualMenuPressed(_ sender: Any) {
        viewContextualMenu.isHidden = false
    }*/
    
    @IBAction func bnFurlan(_ sender: AnyObject) {
        
        _tipoRicerca = TipoRicerca.fur
        _paginaCorrente = 1;
        _list.removeAll()
        InviaRichiestaRicerca(String(_paginaCorrente))
    }
    
    func InviaRichiestaRicerca(_ pagina: String){
    
        dismissKeyboard()
        
        var searchWord = etRicerca.text!
        searchWord = TextUtils.adjustWord(searchWord)
        
        if(searchWord.characters.count == 0){
            _list.removeAll()
            tvRicerca.reloadData()
            return;
        }
        
        var query : String?
            
        if(_tipoRicerca == TipoRicerca.fur){
            query = ApplicationSetup.ENGINEURL+"searchfr?st="+searchWord+"&page="+pagina;
        }
        else if(_tipoRicerca == TipoRicerca.ita){
            query = ApplicationSetup.ENGINEURL+"search?st="+searchWord+"&page="+pagina;
        }
        
        let encoded = query!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        let url = URL(string: encoded!)
        
        if(_task != nil){
            _task?.cancel()
            self.messageFrame.removeFromSuperview()
        }
        
        if(!ConnectivityHelper.isInternetAvailable()){
        
            ToastHelper.showAlert(backgroundColor: ApplicationHelper.hexStringToUIColor("#44CECECE"), textColor: UIColor.black, message: "Offline")
            
            var result = ApplicationHelper.getPreference(query!)

            var data = result.data(using: .utf8)!
            
            if(self._tipoRicerca == TipoRicerca.fur){
                self.parseSearch_FUR(data)
            }
            else if(self._tipoRicerca == TipoRicerca.ita){
                self.parseSearch_ITA(data)
            }
            
            DispatchQueue.main.async {
                self.tvRicerca.reloadData()
                self.messageFrame.removeFromSuperview()
            }

        }
        else{
        
            self.progressBarDisplayer("Attendere", true)
            self.tvRicerca.allowsSelection = false
            
            _task = URLSession.shared.dataTask(with: url!) { data, response, error in
                
                guard error == nil else {
                    print(error)
                    return
                }
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                
                let strData = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String
                //ApplicationHelper.setPreference(query!, strData!)
                CacheFifo.putCache(query!, strData!)
                
                if(self._tipoRicerca == TipoRicerca.fur){
                   self.parseSearch_FUR(data)
                }
                else if(self._tipoRicerca == TipoRicerca.ita){
                    self.parseSearch_ITA(data)
                }
                
                DispatchQueue.main.async {
                    self.tvRicerca.allowsSelection = true
                    self.tvRicerca.reloadData()
                    self.messageFrame.removeFromSuperview()
                }
            }
            
            _task!.resume()
            
        }

    }
   
    
    func parseSearch_ITA(_ data : Data?)
    {
        do
        {
            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject]
            
            setupPaging(json)
            setupSuggestion(json)
            
            let joResults = json["results"] as! NSArray
            
            for anItem in joResults as! [[String:AnyObject]]{
                let lemma = anItem["lemma"] as! String
                let uid = anItem["uid"] as! String
                let type = anItem["type"] as? Int
               
                var r = Ricerca()
                
                r.tipoRicerca = TipoRicerca.ita
                r.text = lemma
                r.uid = uid
                r.type = String(type!)
                r.paging = false
                
                _list.append(r)
            }
            
            addPaging()

        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    func parseSearch_FUR(_ data : Data?)
    {
        do
        {
            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:AnyObject]
            
            setupPaging(json)
            setupSuggestion(json)
            
            let joResultsArray : NSArray = json["results"] as! NSArray
            
            for anItem in joResultsArray {
                
                let joResultsArray1 : NSArray = anItem as! NSArray
                
                var lemmaFur = joResultsArray1[0] as! String
                
                var r = Ricerca()
                r.tipoRicerca = TipoRicerca.fur
                r.text = lemmaFur
                
                let joResultsArray2 = joResultsArray1[2] as! NSArray
                
                for anItem2 in joResultsArray2 as! [[String:AnyObject]]{
                
                    let lemma = anItem2["lemma"] as! String
                    let uid = anItem2["uid"] as! String
                    let type = anItem2["type"] as? String
                
                    var lemmaObj = Lemma()
                    lemmaObj.id = uid
                    lemmaObj.parola = lemma
                    
                    r.lemmi.append(lemmaObj)
                }
                
                _list.append(r)
                
            }
            
            addPaging()
            
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    func addPaging(){
    
        if(_paginaCorrente < _pagineTotali){
            
            var rPagina = Ricerca()
            rPagina.paging = true
            
            _list.append(rPagina)
        }

    }
    
    func setupPaging(_ json: [String: AnyObject]){
    
        let jo = json["paging"] as! [String: AnyObject]
        
        _paginaCorrente = jo["current_page"] as! Int
        _pagineTotali = jo["total_pages"] as! Int
    }
    
    func setupSuggestion(_ json: [String:AnyObject]){
    
        //let suggestion = json["is_suggestion"] as! [String: AnyObject]
        
        if(json["is_suggestion"] != nil){
            var suggestion = json["is_suggestion"] as! Int
            
            if(suggestion == 1)
            {
                var ricerca = Ricerca()
                ricerca.text = "Peraule no cjatade, cirivistu forsit ...?"
                ricerca.uid = ""
                ricerca.tipoRicerca = TipoRicerca.undefined
                ricerca.paging = false
                
                _list.append(ricerca);
            }
        }
    }
    
    func incrementaPagina(){
    
        if(_paginaCorrente+1 <= _pagineTotali){
            _paginaCorrente += 1;
        }
        
        _list.removeLast()
        
        InviaRichiestaRicerca(String(_paginaCorrente));
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "RicercaTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RicercaTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let ricerca = _list[(indexPath as NSIndexPath).row]
        
        cell.setRicerca(ricerca)
        cell.setup()
        
        if ((indexPath as NSIndexPath).row % 2 == 0){
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#e0dad1")
        } else {
            cell.backgroundColor = ApplicationHelper.hexStringToUIColor("#ede9e4")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow!
        
        let currentCell = tableView.cellForRow(at: indexPath) as! RicercaTableViewCell
        
        let ricerca = _list[(indexPath as NSIndexPath).row]
        
        if(ricerca.paging){
        
            incrementaPagina()
        }
        else{
        
            if(ricerca.tipoRicerca == RicercaViewController.TipoRicerca.fur){
            
                self.performSegue(withIdentifier: "LemmiViewSegue", sender: ricerca)
            }
            else if(ricerca.tipoRicerca == RicercaViewController.TipoRicerca.ita){
               
                self.performSegue(withIdentifier: "DefinizioneViewSegue", sender: ricerca)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.destination is DefinizioneViewController){
        
            let destinationVC = segue.destination as? DefinizioneViewController
            
            var ricerca = sender as? Ricerca
            
            var lemma =  Lemma()
            lemma.id = ricerca?.uid
            lemma.parola = ricerca?.text
            
            destinationVC?.lemma = lemma
        }
        else if(segue.destination is LemmiViewController){
            
            let destinationVC = segue.destination as? LemmiViewController
            destinationVC?.ricerca = sender as? Ricerca
        }
    }
    
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()

    func progressBarDisplayer(_ msg:String, _ indicator:Bool ) {
        //print(msg)
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.white
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        if indicator {
            activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            activityIndicator.startAnimating()
            messageFrame.addSubview(activityIndicator)
        }
        messageFrame.addSubview(strLabel)
        view.addSubview(messageFrame)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

}

