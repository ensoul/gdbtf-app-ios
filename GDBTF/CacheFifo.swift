//
//  CacheFifo.swift
//  GDBTF
//
//   08/05/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation

class CacheFifo{

    // Cyclic cache
    //    key1,key2,key3
    //
    //   if(key.count > 500){
    //        keys.remove(key1)
    //        //-> key2,key3
    //    }

    static let LIMIT = ApplicationSetup.DEBUG ? 5 : 500
    static let KEYS = "KEYS"
    static let CACHE_FIFO = "CACHE_FIFO"
    
    static func getKeys() -> [String]
    {
        var list:[String] = [String]()
        var keys = ApplicationHelper.getPreference(KEYS)
        
        if(TextUtils.isEmpty(keys)){
            return list;
        }
        
        list = keys.components(separatedBy: ",")
        
        return list
    }
    
    static func keysListToString(_ keyList:[String]) -> String
    {
        var result = ""
        
        for s in keyList{
            result += s + ","
        }
        
        return result
    }
    
    static func putCache(_ key:String, _ value:String)
    {
        setupCache()
    
        var keyList = getKeys()

        if(   !keyList.contains(key)
           && keyList.count > LIMIT ){
            
            var firstKey = keyList[0]
            keyList.remove(at: 0)
            
            ApplicationHelper.removeSharedPreferences(firstKey)
            
        }
        ApplicationHelper.setPreference(KEYS, keysListToString(keyList) + key)
        ApplicationHelper.setPreference(key,value);
    }
    
    static func setupCache(){
    
        var CACHEFIFO = ApplicationHelper.getPreference(CACHE_FIFO)
    
        if(TextUtils.isEmpty(CACHEFIFO) ){
            ApplicationHelper.clearPreferences()
            ApplicationHelper.setPreference(CACHE_FIFO, "1")
        }
    }

}
