//
//  RicercaCellTableViewCell.swift
//  GDBTF
//
//   19/07/16.
//  
//

import UIKit

class RicercaTableViewCell: UITableViewCell {


    @IBOutlet weak var lbAltriRisultati: UILabel!
    @IBOutlet weak var lbLemma: UILabel!
    @IBOutlet weak var ricerca_iv: UIImageView!

    var _ricerca: Ricerca!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setRicerca(_ ricerca: Ricerca)
    {
        _ricerca = ricerca;
    }
    
    func setup(){
    
        var fontNormal = UIFont.systemFont(ofSize: 17.0)
        var fontItalic = UIFont.italicSystemFont(ofSize: 17.0)
        var fontBold = UIFont.boldSystemFont(ofSize: 17.0)
        lbLemma.font = fontNormal
        
        if(!_ricerca.paging){
            
            if(_ricerca.tipoRicerca == RicercaViewController.TipoRicerca.ita){
            
                var value = TextUtils.setupApex(_ricerca.text!)
                
                if(_ricerca.type == "1"){
                    ricerca_iv.image = UIImage(named: "definizione_poli")
                    ricerca_iv.isHidden = false
                    lbLemma.font = fontBold
                }
                else if(_ricerca.type == "2"){
                    ricerca_iv.isHidden = true
                    lbLemma.font = fontItalic
                }
                
                lbLemma.text = value
            }
            else if(_ricerca.tipoRicerca == RicercaViewController.TipoRicerca.fur){
                
                let res = String(format: "%@ (%d)", _ricerca.text!, _ricerca.lemmi.count)
                
                lbLemma.font = fontNormal
                
                lbLemma.text = res
                ricerca_iv.isHidden = true
//                ricerca_iv.image = UIImage(named: "definizione_fur")
            }
            else
            {
                ricerca_iv.isHidden = true
                lbLemma.text = _ricerca.text!
            }
            
            lbLemma.isHidden = false
            lbAltriRisultati.isHidden = true
        }
        else{
            
            lbLemma.isHidden = true
            ricerca_iv.isHidden = true
            lbAltriRisultati.isHidden = false
        }
    }
    

}
