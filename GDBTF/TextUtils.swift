//
//  TextUtils.swift
//  GDBTF
//
//   20/10/16.
//  
//

import Foundation

class TextUtils{

    static func joinString(_ value:String) -> String{

        var result = value + ""
     
        return result
    }
    
    static func adjustWord(_ value:String) -> String{
    
        var result = value
        result = result.replacingOccurrences(of: "\\s+", with: " ")
        result = result.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        result = result.replacingOccurrences(of: " ", with: "%20")
        
        return result
    }
    
    static func removeBrackets(_ value:String) -> String{
    
        var result = value
        result = result.replacingOccurrences(of: "[", with: "")
        result = result.replacingOccurrences(of: "]", with: "")
        
        return result
    }
    
    static func removeQuotations(_ value:String) -> String{
    
        var result = value
        result = result.replacingOccurrences(of: "\"", with: "")
        
        return result
    }
    
    static func isEmpty(_ value:String?) -> Bool{
    
        if(value == nil){
            return true
        }
        
        return value!.isEmpty
    }
    
    static func indexFirst(where text:String, of occurence:String) -> Int {
        
        let range: Range<String.Index> = text.range(of: occurence)!
        let index: Int = text.distance(from: text.startIndex, to: range.lowerBound)
        
        return index - 1
    }
    
    static func indexLast(where text:String, of occurence:String) -> Int {
        
        let range: Range<String.Index> = text.range(of: occurence)!
        let index: Int = text.distance(from: text.startIndex, to: range.lowerBound)
        
        return index + occurence.characters.count
    }
    
    static func getApex(number n:String) -> String{
    
        if(n == "1"){
            return "\u{00B9}"
        }
        else if(n == "2"){
            return "\u{00B2}"
        }
        else if(n == "3"){
            return "\u{00B3}"
        }
        else if(n == "4"){
            return "\u{2074}"
        }
        else if(n == "5"){
            return "\u{2075}"
        }
        else if(n == "6"){
            return "\u{2076}"
        }
        else if(n == "7"){
            return "\u{2077}"
        }
        else if(n == "8"){
            return "\u{2078}"
        }
        else if(n == "9"){
            return "\u{2079}"
        }
        else if(n == "0"){
            return "\u{2070}"
        }
    
        return n
    }
    
    static func setupApex(_ value:String) -> String{
        
        var result = value;
        
        if(result.range(of: ")") != nil){
            
            let idx1 = result.index(of: "(")!
            let idx2 = result.index(of: ")")!
            let rangeApex = result.index(after: idx1)..<idx2
            
            let apex = result[rangeApex]
            let apex2 = TextUtils.getApex(number: String(apex))
            
            let range = result.index(after:idx2)..<value.endIndex
            result = result[range] + apex2
        }
        
        return result
    }
    
    static func toRoman(number: Int) -> String {
        
        let romanValues = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
        let arabicValues = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        
        var romanValue = ""
        var startingValue = number
        
        for (index, romanChar) in romanValues.enumerated() {
            
            var arabicValue = arabicValues[index]
            
            var div = startingValue / arabicValue
            
            if (div > 0)
            {
                for j in 0..<div
                {
                    //println("Should add \(romanChar) to string")
                    romanValue += romanChar
                }
                
                startingValue -= arabicValue * div
            }
        }
        
        return romanValue
    }
}
