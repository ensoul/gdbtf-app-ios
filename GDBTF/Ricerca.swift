//
//  Ricerca.swift
//  GDBTF
//
//   26/07/16.
//  
//

import Foundation

class Ricerca{

    static let LEMMA_MONOREMATICO = 1;
    static let LEMMA_POLIREMATICO = 2;
    static let CONFISSO = 4;
    
    var tipoRicerca: RicercaViewController.TipoRicerca?
    
    var text: String?
    var uid: String?
    var type: String?
    
    var page = ""
    var paging = false
    
    var lemmi:[Lemma] = [Lemma]()

}
