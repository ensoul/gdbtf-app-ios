//
//  ApplicationSetup.swift
//  GDBTF
//
//   13/09/16.
//  
//

import Foundation

class ApplicationSetup{

    static let ADVIMG = "https://www.arlef.it/app/splash.jpg" //"http://www.arlef.it/repository/immagini/banner_librarie_1479806661034686800.jpg"
    static let ENGINEURL = "https://arlef.it/cgi-bin/gdbtf_api.pl/"
    static let LEMMA = "get_lemma?uid="
    
    static let DEBUG = false


}
